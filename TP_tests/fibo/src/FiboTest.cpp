#include <CppUTest/CommandLineTestRunner.h>
#include "Fibo.hpp"

TEST_GROUP(GroupFibo) {};

TEST(GroupFibo, GroupeFibo_test1){
	CHECK_EQUAL( fibo(5), 5);
}
TEST(GroupFibo, GroupeFibo_test2){	
	CHECK_EQUAL( fibo(4), 3);
}
TEST(GroupFibo, GroupeFibo_test3){	
	CHECK_EQUAL( fibo(3), 2);
}
TEST(GroupFibo, GroupeFibo_test4){	
	CHECK_EQUAL( fibo(2), 1);
}
TEST(GroupFibo, GroupeFibo_test5){
	CHECK_EQUAL( fibo(1), 1);
}
