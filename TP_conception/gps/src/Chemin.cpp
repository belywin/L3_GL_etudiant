#include "Chemin.hpp"
#include <limits>
#include <queue>
#include <sstream>
#include <string>
using namespace std;

/// \brief implémentation interne pour le calcul de plus court chemin
/// 
struct Parcours_ {
    Chemin cheminParcouru_;
    Chemin cheminRestant_;
    int distanceParcourue_;
};

Chemin Chemin::calculerPlusCourt(const string & ville1, 
        const string & ville2) const {

    if (routes_.empty())
        throw string("Chemin::calculerPlusCourt : routes_.empty()");

    if (ville1 == ville2)
        throw string("Chemin::calculerPlusCourt : ville1 == ville2");

    Chemin meilleurChemin;
    int meilleureDistance = std::numeric_limits<int>::max();
    queue<Parcours_> fileParcours;
    // initialise la file de parcours
    {
        Chemin cheminAvec, cheminSans;
        partitionner(ville1, cheminAvec, cheminSans);
        for (const Route & r : cheminAvec.routes_) {
            Chemin c;
            c.routes_.push_back(r);
            fileParcours.push(Parcours_{c, cheminSans, r.distance_});
        }
    }

    // teste tous les parcours
    while (not fileParcours.empty()) {
        const Parcours_ & parcoursCourant = fileParcours.front();
        const string & villeEtape 
            = parcoursCourant.cheminParcouru_.routes_.back().villeB_;
        if (villeEtape == ville2 
                and parcoursCourant.distanceParcourue_ < meilleureDistance) {
            meilleurChemin = parcoursCourant.cheminParcouru_;
            meilleureDistance = parcoursCourant.distanceParcourue_;
        }
        else if (villeEtape != ville1) {
            Chemin cheminAvec, cheminSans;
            const Chemin & cheminRestant = parcoursCourant.cheminRestant_;
            cheminRestant.partitionner(villeEtape, cheminAvec, cheminSans);
            for (const Route & r : cheminAvec.routes_) {
                Chemin c(parcoursCourant.cheminParcouru_);
                c.routes_.push_back(r);
                int d = parcoursCourant.distanceParcourue_ + r.distance_;
                fileParcours.push(Parcours_{c, cheminSans, d});
            }
        }
        fileParcours.pop();
    }

    return meilleurChemin;
}

void Chemin::partitionner(const string & ville, Chemin & cheminAvec, 
        Chemin & cheminSans) const {
	for (const Route &route : routes_)
	{
		if(route.villeA_ == ville)
		{
			cheminAvec.routes_.push_back(Route{ville,route.villeB_,route.distance_});
		} else if(route.villeB_ == ville)
		{
			cheminAvec.routes_.push_back(Route{ville,route.villeA_,route.distance_});
		}
		else {
			cheminSans.routes_.push_back(Route{route.villeA_,route.villeB_,route.distance_});
		}
	}
    // TODO

}

void Chemin::importerCsv(istream & is) {
	string line;
	while(getline(is,line))
	{
		std::istringstream iss(line);
		std::string villeA,villeB,dist;
		getline(iss,villeA,' ');
		getline(iss,villeB,' ');
		getline(iss,dist,' ');
		Route route;
		route.villeA_ = villeA;
		route.villeB_ = villeB;
		route.distance_ = stoi(dist);
		routes_.push_back(route);
		//if( line.find( ) != string.npos)
		//{
		//	is << line << endl;
		//}
		//return ;
	}
    // TODO

}

void Chemin::exporterDot(ostream & os, const string & ville1, 
        const string & ville2) const {
	os << 'graph { ' << endl;
	os << 'splines=line;' << endl;
	Chemin pc = calculerPlusCourt(ville1,ville2);
	for(const Route &r: pc.routes_){
	os << r.villeA_ << ' -- ';
	}
	os << ville2 << ' [color=red, penwidth=3];' << endl;
	for(int i = 0; i <= routes_.size() ; i++){
		os << routes_[i].villeA_ << ' -- ' << routes_[i].villeB_ << ' [label=' << routes_[i].distance_ <<'];' << endl;
	}
	os << "}" << endl;
    // TODO

}

