.. drunk_player documentation master file, created by
   sphinx-quickstart on Wed Mar 27 11:17:45 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation de drunk_player
========================================

**Résumé**
+--------------------------------------------------------------------------+
|Drunk_player est un système de lecture vidéos qui a trop bu.              |
|Il lit les vidéos contenues dans un dossier par morceaux,aléatoirement en |
|transformant l'image.                                                     |
+--------------------------------------------------------------------------+
.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

